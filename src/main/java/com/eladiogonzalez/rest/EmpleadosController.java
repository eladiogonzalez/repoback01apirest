package com.eladiogonzalez.rest;

import com.eladiogonzalez.rest.empleados.Capacitacion;
import com.eladiogonzalez.rest.empleados.Empleado;
import com.eladiogonzalez.rest.empleados.Empleados;
import com.eladiogonzalez.rest.repositorios.EmpleadoDAO;
import com.eladiogonzalez.rest.utils.Configuracion;
import com.eladiogonzalez.rest.utils.EstadosPedido;
import com.eladiogonzalez.rest.utils.Utilidades;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados() {
        logger.debug("Empleados devueltos");
        return empDao.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        Empleado emp = empDao.getEmpleado(id);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }
    }
    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empDao.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }
    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        empDao.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }
    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        empDao.updEmpleado(id, emp);
        return ResponseEntity.ok().build();
    }
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id) {
        String resp = empDao.deleteEmpleado(id);
        if (resp == "OK") {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softupdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates) {
        empDao.softupdEmpleado(id, updates);
        return ResponseEntity.ok().build();
    }
    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id) {
        return ResponseEntity.ok().body(empDao.getCapacitacionesEmpleado(id));
    }
    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id, @RequestBody Capacitacion cap) {
        if (empDao.addCapacitacion(id, cap)) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @Value("${app.titulo}") private String titulo;

    @GetMapping("/titulo")
    public String getAppTitulo() {
        String modo = configuracion.getModo();
        return String.format("%s (%s)", titulo, modo);
    }

    @Autowired
    private Environment env;


    @Autowired
    Configuracion configuracion;

    @GetMapping("/author")
    public String getAutor()
    {
        return configuracion.getAutor();
    }

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {
            //EstadosPedido.ACEPTADO
            return Utilidades.getCadena(texto, separador);
        } catch (Exception e) {
            return "";
        }
    }



}


//  @Component  se utiizará par
// @Service sea tratado como servicio como una
// @Repository   persistencia al contenido de la infomación almacena y obtiene información