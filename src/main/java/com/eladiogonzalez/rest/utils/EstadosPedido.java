package com.eladiogonzalez.rest.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINANDO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO,
    COMIDO;
}
