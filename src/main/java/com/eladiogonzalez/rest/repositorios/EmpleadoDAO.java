package com.eladiogonzalez.rest.repositorios;


import com.eladiogonzalez.rest.empleados.Capacitacion;
import com.eladiogonzalez.rest.empleados.Empleado;
import com.eladiogonzalez.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadoDAO {
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static {
        Capacitacion cap1 = new Capacitacion("2020/01/01", "DBA");
        Capacitacion cap2 = new Capacitacion("2019/12/01", "backend");
        Capacitacion cap3 = new Capacitacion("2021/10/01", "java");
        Capacitacion cap4 = new Capacitacion("2018/11/11", "practitionar");
        Capacitacion cap5 = new Capacitacion("2019/05/20", "bbva");
        ArrayList<Capacitacion>  una = new ArrayList<Capacitacion>();
        una.add(cap1);
        ArrayList<Capacitacion>  dos = new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);
        ArrayList<Capacitacion>  tres = new ArrayList<Capacitacion>();
        tres.add(cap1);
        tres.add(cap2);
        tres.add(cap3);

        ArrayList<Capacitacion>  todas = new ArrayList<Capacitacion>();
        todas.add(cap4);
        todas.add(cap5);
        todas.addAll(tres);



        // información estatico para la funcion
        list.getListaEmpleados().add(new Empleado(1, "Antonio", "Lopez", "antonio@lopez.com", una));
        list.getListaEmpleados().add(new Empleado(2, "Eladio", "Gonzalez", "eladio@gonzalez.com", dos));
        list.getListaEmpleados().add(new Empleado(3, "Eduardo", "Sandoval", "eduardo@sandoval.com", tres));
        list.getListaEmpleados().add(new Empleado(4, "Flerida", "Rojas", "flerida@rojas.com", todas));

    }
    //mostrar todos los empleados lista
    public Empleados getAllEmpleados(){
        logger.debug("Empleados Devueltos");
        return list;

    }
    // mostrar un empleado
    public Empleado getProducto(int id){
        for (Empleado emp: list.getListaEmpleados()){
            if (emp.getId() == id){
                return emp;
            }
        }
        return null;
    }
    // función para añadir un empleado funcion publica no devuelve nada
    public void addEmpleado(Empleado emp){
        list.getListaEmpleados().add(emp);
        // agregar el id de forma automática

    }
    public void updEmpleado(Empleado emp){
        Empleado current = getEmpleado(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
        //list.getListaEmpleados().add(emp);
        // agregar el id de forma automática

    }
     public void  updEmpleado(int id, Empleado emp){
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
        //list.getListaEmpleados().add(emp);
        // agregar el id de forma automática

    }
    // funcion para borrar y gestionamos el error en caso de no existir, mostrar not ofund
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if (current == null) return "Not Found";
        Iterator it = list.getListaEmpleados().iterator();
        // si usamos for no podremos eliminar el registro, por eso el uso del while, ya que usamos el iterator
        // no se puede modificar en el condicional del bucle
        while (it.hasNext()){  // hasnext devuelve true si existe otro registro
            Empleado emp = (Empleado) it.next();
            // convertimos el objeto a tipo empleado
            if (emp.getId() == id) {
                it.remove();
                break;
            }
        }
        return "OK";
    }
    public void softupdEmpleado(int id, Map<String, Object> updates){
        Empleado current = getEmpleado(id);
        for (Map.Entry<String, Object> update :updates.entrySet()) {
            switch (update.getKey()){
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;
            }
        }
    }
    //funcion para devoilver lista de objeto capacitacion
    public List<Capacitacion> getCapacitacionesEmpleado(int id){
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>();
        if (current != null) caps = current.getCapacitaciones();
        return caps;
    }
    // agregar capacitación recibiendo el id del empleado
    public Boolean addCapacitacion(int id, Capacitacion cap){
        Empleado current = getEmpleado(id);
        if (current == null) return false;      // devuelve false en caso de no encontrar
        current.getCapacitaciones().add(cap);   // se graga la capacitación recibida
        return true;                            // devuelve true al añadairla
    }


}
