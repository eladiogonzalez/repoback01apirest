package com.eladiogonzalez.rest;


import com.eladiogonzalez.rest.utils.BadSeparator;
import com.eladiogonzalez.rest.utils.EstadosPedido;
import com.eladiogonzalez.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;


@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen, "."));
    }
    @Test
    public void testSeparadorGetCadena(){
        try{
            //Utilidades.getCadena("Eladio Gonzalez", ",");
            Utilidades.getCadena("Eladio  Gonzalez", " ");
            fail("Se esperaba BadSeparator");
        } catch (BadSeparator bs){
            System.out.println("nada");
        }
    }

    /*
    @Test
    public void testGetAutor(){
        assertEquals("Eladio J. Gonzalez", empleadosController.getAutor());
    }
    /*
    @ParameterizedTest
    // @ParameterizedTest se utiliza para realizar testeo parametrizado
    @ValueSource(ints = {1,3,5,15,-3, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    /*@ParameterizedTest
    //@EmptySource
    @NullAndEmptySource  // comprueba null y cadena vacia
    @ValueSource(ints = {1, 3, 5, 15, -3, Integer.MAX_VALUE})
    public void testEsImpar1(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }*/
    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {" ", "\t", "\n"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }
    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }
}
